import scipy.io as spio
import numpy as np
from PIL import Image
import cv2
from sklearn.model_selection import train_test_split
import visualize

def get_annotations():

    '''
    the following 3 functions are to convert the .mat file into a python dictionnary. The scpio.loadmat function
    does not support nested mat-objects therefore it cannot be used.
    '''      
    def loadmat(filename):
        data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
        return _check_keys(data)

    def _check_keys(dict):
        for key in dict:
            if isinstance(dict[key], spio.matlab.mat_struct):
                dict[key] = _todict(dict[key])
        return dict        

    def _todict(matobj):
        '''
        A recursive function which constructs from matobjects nested dictionaries
        '''
        dict = {}
        for strg in matobj._fieldnames:
            elem = matobj.__dict__[strg]
            if isinstance(elem, spio.matlab.mat_struct):
                dict[strg] = _todict(elem)
            else:
                dict[strg] = elem
        return dict


    mat = loadmat('mpii_human_pose_v1_u12_1.mat')

    raw_annotations = list(map(_todict, mat['RELEASE']['annolist']))

    #Isolate the annotations that have 16 points defined (all 16 nodes are properly annotated) view dataset website in readme for structure information
    valid_annotations_full = []
    for item in raw_annotations:
        if 'annorect' in item:
            if type(item['annorect']) is dict:
                if 'annopoints' in item['annorect']:
                    if 'point' in item['annorect']['annopoints']:
                        if len(list(map(_todict, item['annorect']['annopoints']['point']))) == 16:
                            valid_annotations_full.append(item)

    #Iterate over valid annotations. Rename resize and save images, and isolate x,y coordinates of the head (id: 9)
    index = 0
    valid_annotations_min = []
    for annotation in valid_annotations_full:
        name = annotation['image']['name']
        points = list(map(_todict, annotation['annorect']['annopoints']['point']))
        #Resize and save image
        img = Image.open('./images_MPII/' + name)
        x_ratio = img.size[0] / float(320.0)
        y_ratio = img.size[1] / float(320.0)
        #Update Coordinates
        for point in points:
            point['x'] = point['x'] / x_ratio
            point['y'] = point['y'] / y_ratio

        #Sort the points by their node IDs
        points_sorted = sorted(points, key=lambda d: d['id'])
        '''
        The following code only adds thorax and wrist nodes. To train the model for all 16 nodes
        just add all 16 x,y,is_visible values in this format [[x1,y1,is_visible1],[x2,y2,is_visible2],...,[x16,y16,is_visible16]]
        '''
        valid_annotations_min.append([points_sorted[7],points_sorted[10],points_sorted[15]])
        '''
        uncomment the following lines if you are running this program for the first time and have not saved formatted images yet
        '''
        # img = img.resize((320,320), Image.ANTIALIAS)
        # img.save('./images_MPII_formatted/' + str(index) + '.jpg')
        # index += 1
    return valid_annotations_min

def create_gaussians(annotations):
    empty_gaussian = np.zeros(shape=(320,320,1))
    gaussians = []
    #this function is to convert [{x,y,is_visible},{x,y,is_visible}...] -> (320,320,X) gaussian 
    for annotation in annotations:
        if (annotation['is_visible'] == 1):
            gaussians.append(generate_gaussian(center=(int(annotation['x']), int(annotation['y']))))
        else:
            gaussians.append(empty_gaussian)

    combined_gauss = np.concatenate(gaussians, axis=2)
    return combined_gauss  


def generate_gaussian(size=(320,320), center=(0,0), sigma=20):
    #Returns a 320,320,1 gaussian with the provided center and a radius of sigma
    x = np.arange(0, size[0], 1, float)
    y = np.arange(0, size[1], 1, float)
    x = x[:, np.newaxis]
    y = y[np.newaxis, :]
    arr = np.exp(-((x-center[1])**2 + (y-center[0])**2) / (2*sigma**2))
    return np.reshape(arr, (320,320,1))

y_all = get_annotations()
x_all = []

for i in range(len(y_all)):
    #converting the annotations to gaussians 
    y_all[i] = create_gaussians(y_all[i])

for i in range(len(y_all)):
    #adding formatted image
    img_path = './images_MPII_formatted/' + str(i) + '.jpg'
    img = cv2.imread(img_path)
    x_all.append(img)

x_all = np.array(x_all)

# WARNING: train_test_split is fairly memory constrained and will not work with larger x_all and y_all
x_train, x_valid, y_train, y_valid = train_test_split(x_all, y_all, test_size=0.15, random_state=123)

np.save('x_train.npy', x_train)
np.save('x_valid.npy', x_valid)
np.save('y_train.npy', y_train)
np.save('y_valid.npy', y_valid)