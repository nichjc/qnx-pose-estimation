from matplotlib import pyplot as plt
import tensorflow as tf
import numpy as np

#Input: 2 arrays representing an image and it's heatmap
def display(images):
    plt.figure(figsize=(15,15))
    title = ['Input Image', 'heatmap']
    for i in range(len(images)):
        plt.subplot(1, len(images), i+1)
        plt.title(title[i])
        plt.imshow(tf.keras.utils.array_to_img(images[i]))
        plt.axis('off')
    plt.show()

#Input: image and output heatmaps, displays each heatmap alongside the image
def display_multi(image, heatmaps):
    plt.figure(figsize=(15,15))
    #flatten heatmaps
    heatmaps = np.split(heatmaps,indices_or_sections=2, axis=2)
    heatmaps = np.asarray(heatmaps)
    title = ['thorax', 'r wrist', 'l wrist']
    plt.subplot(1, len(heatmaps) + 1, 1)
    plt.title('Input Image')
    plt.imshow(tf.keras.utils.array_to_img(image))
    plt.axis('off')
    for i in range(len(heatmaps)):
        plt.subplot(1, len(heatmaps) + 1, i+2)
        plt.title(title[i])
        plt.imshow(tf.keras.utils.array_to_img(heatmaps[i]))
        plt.axis('off')
    plt.show()

#Input: image and heatmaps, output will be all heatmaps overlayed (not very effective with many nodes)
def display_multi_overlay(image, heatmaps):
    #flatten heatmaps
    title = ['thorax', 'r wrist', 'l wrist']
    heatmaps = np.split(heatmaps,indices_or_sections=3, axis=2)
    heatmaps = np.asarray(heatmaps)
    for i in range(len(heatmaps)):
        display_overlay(image,heatmaps[i],title[i])

#display single heatmap overlayed onto image
def display_overlay(image, heatmap, title="title"):
    plt.figure(figsize=(5, 5))
    plt.title(title)
    plt.imshow(tf.keras.utils.array_to_img(image))
    plt.imshow(tf.keras.utils.array_to_img(heatmap), alpha=0.5)
    plt.show()
    _ = plt.axis('off')
