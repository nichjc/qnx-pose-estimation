import tensorflow as tf
from tensorflow.keras.layers import Conv2D, BatchNormalization, UpSampling2D, Add, Activation

def hourglass_module(x, num_features):
    # Downsample
    x_down = Conv2D(num_features, (3, 3), strides=(2, 2), padding='same')(x)
    x_down = BatchNormalization()(x_down)
    x_down = Activation('relu')(x_down)
    
    # Upsample
    x_up = UpSampling2D((2, 2))(x_down)
    
    return x_down, x_up

def build_hourglass_model(input_shape, num_stacks, num_features=128, num_keypoints=2):
    inputs = tf.keras.Input(shape=input_shape)
    x = inputs
    
    # Initial processing
    x = Conv2D(num_features, (7, 7), strides=(2, 2), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    stack_outputs = []
    
    # Stacked hourglass modules
    for _ in range(num_stacks):
        x_down, x_up = hourglass_module(x, num_features)
        
        # Residual connection
        x = Add()([x, x_up])
        
        # Store intermediate stack output
        stack_outputs.append(x_down)
    
    # Final layers to generate heatmap output
    x = Conv2D(num_keypoints, (1, 1), activation='sigmoid', padding='same')(x)
    
    # Upsample to desired output size
    output = UpSampling2D(size=(2, 2))(x)
    
    model = tf.keras.Model(inputs=inputs, outputs=output)
    
    return model
