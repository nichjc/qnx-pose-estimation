import tensorflow as tf
from model_arch_02 import build_hourglass_model
#from model_arch_01 import model
import numpy as np
import matplotlib as plt

# Load data
x_train = np.load('x_train.npy')
y_train = np.load('y_train.npy')
x_valid = np.load('x_valid.npy')
y_valid = np.load('y_valid.npy')

#WARNING memory growth does not work with arch_01 since model is already defined
physical_devices = tf.config.list_physical_devices('GPU')
if physical_devices:
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)
        
# Define model parameters for arch_02
input_shape = (320, 320, 3)
num_stacks = 2
num_features = 256
num_keypoints = 3  # Change this to the number of nodes your estimating

model = build_hourglass_model(input_shape, num_stacks, num_features, num_keypoints)

model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), 
             loss=tf.keras.losses.MeanSquaredError())

#Callback to save model after each EPOCH
class DisplayCallback(tf.keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs=None):
    name = 'models/PE' + str(epoch)
    model.save(name)
    print ('\nModel saved after epoch {}\n'.format(epoch+1))

#Training params (larger batch size is preferable for system with more memory)
TRAIN_LENGTH = len(x_train)
BATCH_SIZE = 8
BUFFER_SIZE = 1000
STEPS_PER_EPOCH = TRAIN_LENGTH // BATCH_SIZE

# Using simple generators to form a tensorflow dataset. This is done so that the input is fed to the model sequentially and the dataset is not loaded all at once resulting in OOM
def train_dataset_gen():
    for i in range(len(x_train)):
        yield x_train[i], y_train[i]

def valid_dataset_gen():
    for i in range(len(x_valid)):
        yield x_valid[i], y_valid[i]

train_ds = tf.data.Dataset.from_generator(
    train_dataset_gen,
    output_signature=(
        tf.TensorSpec(shape=(320, 320, 3), dtype=tf.float32, name=None), tf.TensorSpec(shape=(320, 320, 3), dtype=tf.float64, name=None)))

valid_ds = tf.data.Dataset.from_generator(
    valid_dataset_gen,
    output_signature=(
        tf.TensorSpec(shape=(320, 320, 3), dtype=tf.float32, name=None), tf.TensorSpec(shape=(320, 320, 3), dtype=tf.float64, name=None)))

train_batches = (
    train_ds
#shuffling can be included however it is a very expensive operation and may cause OOM
#    .shuffle(BUFFER_SIZE)
    .batch(BATCH_SIZE)
    .repeat()
    .prefetch(buffer_size=tf.data.AUTOTUNE))

valid_batches = valid_ds.batch(BATCH_SIZE)

EPOCHS = 100
VAL_SUBSPLITS = 5
VALIDATION_STEPS = len(x_valid) // BATCH_SIZE // VAL_SUBSPLITS

history = model.fit(train_batches, epochs=EPOCHS,
                          steps_per_epoch=STEPS_PER_EPOCH,
                          validation_steps=VALIDATION_STEPS,
                          validation_data=valid_batches,
                          callbacks=[DisplayCallback()])

model.save('models/pose_estimation')

'''
Code to convert model to tflite
'''
# print("saving and converting the model")
# converter = tf.lite.TFLiteConverter.from_keras_model(model)
# tflite_model = converter.convert()

# with open('qnxPose.tflite', 'wb') as f:
#     f.write(tflite_model)