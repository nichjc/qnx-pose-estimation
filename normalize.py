#sometimes I used normalize training data and sometimes not the results varied depending on the value of the gaussians
import numpy as np

x_train = np.load('x_train.npy')
x_valid = np.load('x_valid.npy')

x_train = x_train / 255.0
x_valid = x_valid / 255.0

np.save('x_valid.npy', x_valid)
np.save('x_train.npy', x_train)
