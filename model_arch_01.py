import tensorflow as tf 

inputs = tf.keras.Input(shape=(320,320,3))

convDown1 = tf.keras.layers.Conv2D(
    filters=16,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(inputs)

convPool1 = tf.keras.layers.MaxPooling2D(
    pool_size=[2, 2],
    strides=2,
    padding='same')(convDown1)

convDown2 = tf.keras.layers.Conv2D(
    filters=32,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convPool1)

convPool2 = tf.keras.layers.MaxPooling2D(
    pool_size=[2, 2],
    strides=2,
    padding='same')(convDown2)

convDown3 = tf.keras.layers.Conv2D(
    filters=64,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convPool2)

convPool3 = tf.keras.layers.MaxPooling2D(
    pool_size=[2, 2],
    strides=2,
    padding='same')(convDown3)

convDown4 = tf.keras.layers.Conv2D(
    filters=128,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convPool3)

convTrans1 = tf.keras.layers.Conv2DTranspose(
    filters=64,
    kernel_size=[3, 3],
    strides=(2,2),
    padding='same')(convDown4)

#Skip connection
skipCon1 = tf.keras.layers.Concatenate([convTrans1, convDown3])

convUp1 = tf.keras.layers.Conv2D(
    filters=64,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convTrans1)

convTrans2 = tf.keras.layers.Conv2DTranspose(
    filters=32,
    kernel_size=[3, 3],
    strides=(2,2),
    padding='same')(convUp1)

skipCon2 = tf.keras.layers.Concatenate([convTrans2, convDown2])

convUp2 = tf.keras.layers.Conv2D(
    filters=32,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convTrans2)

convTrans3 = tf.keras.layers.Conv2DTranspose(
                                    filters=16,
                                    kernel_size=[3, 3],
                                    strides=(2,2),
                                    padding='same')(convUp2)

skipCon3 = tf.keras.layers.Concatenate([convTrans3, convDown1])

convEnd1 = tf.keras.layers.Conv2D(
    filters=16,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convTrans3)

convEnd2 = tf.keras.layers.Conv2D(
    filters=16,
    kernel_size=[3, 3],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convEnd1)

outputs = tf.keras.layers.Conv2D(
    filters=3,
    kernel_size=[1, 1],
    strides=[1, 1],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer='l2',
    kernel_initializer=tf.keras.initializers.glorot_normal)(convEnd2)

model = tf.keras.Model(inputs=inputs, outputs=outputs, name="2D_pose_estimation")
